#include <mcp2515.h> 


struct can_frame canMsg1;
// constexpr size_t sizeofXX = sizeof(canMsg1);

struct TestStruct {
  unsigned long jeden;
  char dwa;
  char trzy[8] __attribute__((aligned(8)));
};

void setup() {
  Serial.begin(9600);
}

void loop() {
  canMsg1.can_id  = 0x0F6;
  canMsg1.can_dlc = 8;
  canMsg1.data[0] = 'x';
  canMsg1.data[1] = 'd';
  canMsg1.data[2] = 0x32;
  canMsg1.data[3] = 0xFA;
  canMsg1.data[4] = 'a';
  canMsg1.data[5] = 's';
  canMsg1.data[6] = 'd';
  canMsg1.data[7] = 'f';

  Serial.write( (char*)&canMsg1.can_id , sizeof(canid_t));
  Serial.write( (char*)&canMsg1.can_dlc , sizeof(char));
  Serial.write( (char*)&canMsg1.data , canMsg1.can_dlc);
  // Serial.write()

  delay(500);       
}
