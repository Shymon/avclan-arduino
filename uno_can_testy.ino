#include <mcp2515.h> 
#include "src/AVCLanDriver.h"
#include "src/ToyotaAuxEnabler.c"

// This file is intended for atmega328(uno) based boards only
#ifndef ARDUINO_AVR_UNO
#error This file is intended for atmega328 based boards only (uno check failed)
#endif

void setup() {
  Serial.begin(9600);
	InitMCU();
}

void loop() {
          

    UsartPutCStr( PSTR("\r\n\t\t     Toyota AVC-Lan AUX Enabler\r\n") );
    UsartPutCStr( PSTR("\t\tCopyright (C) 2007, SigmaObjects Inc.\r\n") );


    while ( 1 )
    {
        // Reset watchdog.
        wdt_reset();

        AvcActionID actionID = AvcReadMessage();

        DumpRawMessage( FALSE );

        if ( actionID != ACT_NONE )
        {
            AvcProcessActionID( actionID );
        }
    }
}
