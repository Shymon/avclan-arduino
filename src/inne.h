#pragma once

#include "GlobalDef.h"
#include <Arduino.h>
#include <avr/pgmspace.h>

// #define XD_USART_BUFFER_SIZE       80



void UsartPutStr             ( char *str );
void UsartPutCStr            ( const char *str );