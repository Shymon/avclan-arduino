#include <avr/pgmspace.h>
#include <HardwareSerial.h>
#include "GlobalDef.h"

void UsartPutCStr ( const char *str )
{
    char c;

    while ( (c = pgm_read_byte_near( str++ )) )
    {
        Serial.print( c );
    }
}

void UsartPutStr ( char *str )
{
    while ( *str )
    {
        Serial.print( str );
    }
}